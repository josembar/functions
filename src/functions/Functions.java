package functions;

import java.util.Random;
/**
 *
 * @author Jose
 */
public class Functions {

    public static void main(String[] args) {
        
        System.out.println(reverseString("palo"));
        System.out.println(isPalindrome("jose"));
        System.out.println(isPalindrome("ama"));
        System.out.println(randBetween(5,10));
    }
    
    
    public static String reverseString (String word)
    {
        char[] textToArray = word.toCharArray();
        char[] tempArray = new char[textToArray.length];
        for(int i = textToArray.length - 1; i >= 0; i--)
        {
            tempArray[(textToArray.length - 1) - i] = textToArray[i];
        }
        return new String(tempArray);
    }
    
    public static boolean isPalindrome (String word)
    {
        String reverseWord = reverseString(word);
        return word.equals(reverseWord);
    }
    
    public static int randBetween (int x, int y)
    {
        /*
        Random().ints(origin,bound).findFirst().getAsInt() return a random
        number including the origin and excluding the bound. That why I use
        ints(x,y+1)
        */
        return new Random().ints(x,y+1).findFirst().getAsInt();
    }
}
